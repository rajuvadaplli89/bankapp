import { Component, OnInit } from '@angular/core';
import { BenificiaryService } from 'src/app/ServiceLayer/benificiary.service';
import { ThrowStmt } from '@angular/compiler';
import { Router } from '@angular/router';

@Component({
  selector: 'app-benificiary-account',
  templateUrl: './benificiary-account.component.html',
  styleUrls: ['./benificiary-account.component.css']
})
export class BenificiaryAccountComponent implements OnInit {
  benificiary_account_no: any;
  benificiary_account_name: any;
  benificiary_ifsc_code: any;

  my_benificiary_list: any;
  constructor(private benificiaryservice:BenificiaryService,private router:Router) { }

  ngOnInit(): void {
    this.DisplayMyBenificiaryList();
  }

  saveBenificiary(benificiaryinfo)
  {
    let formdata = {
      "main_account_no": localStorage.getItem("accountno"),
      "beneficiary_account": benificiaryinfo.txt_benificiary_account_no,
      "beneficiary_name": benificiaryinfo.txt_benificiary_account_name,
      "benificiary_ifsc": benificiaryinfo.txt_benificiary_ifsc_code
    }

    this.benificiaryservice.AddBenificiary(formdata).subscribe(data => {
      alert("Benificiary Details Added Successfully...");
      this.clearformdata();
      this.DisplayMyBenificiaryList();
    })

  }

  DisplayMyBenificiaryList()
  {
    let my_account_no = localStorage.getItem("accountno");
    this.benificiaryservice.GetMyBenificiaryList(my_account_no).subscribe(data => {
      this.my_benificiary_list = data;
    })
  }
  Deletebenificiary(benificiary_seq_id,benificiary_name)
  {
    if (confirm("Do you want to Delete Benificiary Name : " + benificiary_name + " ?")==true)
    {
      this.benificiaryservice.DeleteBenificiary(benificiary_seq_id).subscribe(data => {
        alert("Deleted Succesfully");
        this.DisplayMyBenificiaryList();
    })
       
    }
   
  }

  gotofundTransferpage(benificiry_account_no)
  {
    this.benificiaryservice.setBenificiaryAccountnumber(benificiry_account_no);
    this.router.navigate(['/fund-transfer']);
  }

  clearformdata()
  {
    this.benificiary_account_no = '';
    this.benificiary_account_name = '';
    this.benificiary_ifsc_code = '';
  }

}
