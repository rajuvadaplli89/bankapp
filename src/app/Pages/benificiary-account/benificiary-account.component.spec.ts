import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BenificiaryAccountComponent } from './benificiary-account.component';

describe('BenificiaryAccountComponent', () => {
  let component: BenificiaryAccountComponent;
  let fixture: ComponentFixture<BenificiaryAccountComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BenificiaryAccountComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BenificiaryAccountComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
