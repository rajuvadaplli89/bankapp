import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/ServiceLayer/user.service';

@Component({
  selector: 'app-account-summary',
  templateUrl: './account-summary.component.html',
  styleUrls: ['./account-summary.component.css']
})
export class AccountSummaryComponent implements OnInit {
  AccountDetails:any;
  constructor(private userservice:UserService) { }

  ngOnInit(): void {
    this.DisplayAccountDetails()
  }

  DisplayAccountDetails()
  {
    let accountno=localStorage.getItem("accountno");
    this.userservice.getspecificuser(accountno).subscribe(data=>{
      this.AccountDetails=data[0];
    })
  }
}
