import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/ServiceLayer/user.service';
import { AccountService } from 'src/app/ServiceLayer/account.service';
import { BenificiaryService } from 'src/app/ServiceLayer/benificiary.service';

@Component({
  selector: 'app-funds-transfer',
  templateUrl: './funds-transfer.component.html',
  styleUrls: ['./funds-transfer.component.css']
})
export class FundsTransferComponent implements OnInit {

  from_account: any;
  to_account: any;
  amount: any;
  transfer_type: any;
  //enablesendButton: boolean;
  isTwoAccountsNumbers_Valid = false;
  my_balance: any;
  constructor(private userservice: UserService, private accountservice: AccountService,
              private benificiaryservice:BenificiaryService) { }

  ngOnInit(): void {
   
    this.my_balance = localStorage.getItem("totalbalnce");
    this.from_account = localStorage.getItem("accountno");
    this.to_account = this.benificiaryservice.getBenificiaryAccountnumber();
  }

  sendamount(accountinfo) {

    this.CheckLoginUserAccountnumber(accountinfo.txt_from_account);
    this.checkAccountExists(accountinfo.txt_to_account);
    this.CheckAccountBalance(accountinfo.txt_amount);
    
    let formdata = {
      "from_account_no": accountinfo.txt_from_account,
      "to_account_no": accountinfo.txt_to_account,
      "transfer_amount": accountinfo.txt_amount,
      "transfer_type": accountinfo.ddl_transfer_type
    }

    if (this.isTwoAccountsNumbers_Valid == true && accountinfo.txt_amount!=undefined && accountinfo.ddl_transfer_type!=undefined )
    {
      this.accountservice.TransferAmount(formdata).subscribe(data => {
        alert("funds Transfer Done Successfully");
  debugger;
        let account_seq_id=localStorage.getItem('id');
        //calling Update Amount
        let updateformdata={
          "accountno": localStorage.getItem("accountno"),
          "name": localStorage.getItem("name"),
          "password": localStorage.getItem("password"),
          "total_accountbalance": parseInt(this.my_balance) - parseInt(accountinfo.txt_amount)
        }
        this.accountservice.UpdateAccountBalance(account_seq_id,updateformdata).subscribe(data=>{
          
          this.my_balance=data.total_accountbalance;
          debugger;
          localStorage.setItem("totalbalnce",this.my_balance);
          alert("Your Total Balance Updated Successfully");
        })
      });
    }
   
  }

  CheckLoginUserAccountnumber(accountnumber) {
    if (accountnumber != localStorage.getItem("accountno")) {
     // this.enablesendButton = false;
      this.isTwoAccountsNumbers_Valid = false;
      alert("From Account Number should not be Tempared.")
    }
  }
  CheckAccountBalance(amount)
  {
    if (amount > localStorage.getItem("totalbalnce") && this.isTwoAccountsNumbers_Valid==true) {
      
       this.isTwoAccountsNumbers_Valid = false;
      alert("No suffient Amount avaiable..")
      return false;
     }
    
  }

  checkAccountExists(accountnumber) {
    debugger;
    if (accountnumber != '') {
      this.userservice.getspecificuser(accountnumber).subscribe(data => {
        if (data.length == 0) {
        //  this.enablesendButton = false;
          this.isTwoAccountsNumbers_Valid = false;
          alert('Invalid Account Number');
        }
        if (data.length == 1) {
          this.isTwoAccountsNumbers_Valid = true;
         // this.enablesendButton = true;
          //alert('Valid Accunt Number');
        }
      });
    }

  }
}
